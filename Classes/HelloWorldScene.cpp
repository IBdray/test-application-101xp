/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "ui/UIButton.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if (!Scene::init())
        return false;

    const auto director = Director::getInstance();
    const auto visibleSize = director->getVisibleSize();
    const Vec2 origin = director->getVisibleOrigin();


    // Create panelMenu button
    auto panelButton = ui::Button::create("ButtonNormal.png", "ButtonPressed.png");
    if (!panelButton || panelButton->getContentSize().width <= 0 || panelButton->getContentSize().height <= 0)
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    else
    {
        const float x = visibleSize.width - panelButton->getContentSize().width / 2;
		const float y = panelButton->getContentSize().height / 2;
	    panelButton->setPosition(Vec2(x, y));
	    panelButton->setTitleLabel(Label::createWithTTF("Panel", "fonts/Times New Roman.ttf", 40));
	    panelButton->addClickEventListener([&](Ref* sender){togglePanelCallback(sender);});
	    this->addChild(panelButton);
    }

    // Create triangle
    triangle = Sprite::create("Triangle.png");
    if (!triangle)
        problemLoading("'Triangle.png");
    else
    {
        const float x = origin.x + visibleSize.width / 2;
    	const float y = origin.y + visibleSize.height / 3 * 2;
	    triangle->setPosition(x,y);
        triangle->setVisible(false);
    }
    this->addChild(triangle);



    // Create panel
    panel = Sprite::create("Panel.png");
    if (!panel)
		problemLoading("'Panel.png'");
    {
	    const float x = origin.x + visibleSize.width / 2;
    	const float y = origin.y + visibleSize.height / 3;
	    panel->setPosition(x,y);
    	panel->setVisible(false);
    }
    this->addChild(panel);

    auto panelMenu = Menu::create();
    panelMenu->setPosition(Vec2::ZERO);
    panel->addChild(panelMenu);

    // Create panel buttons
    auto triangleRotateButton = createButton("Rotate", CC_CALLBACK_1(HelloWorld::rotateTriangle, this));
    auto triangleScaleButton = createButton("Scale", CC_CALLBACK_1(HelloWorld::scaleTriangle, this));
    auto triangleTranslateButton = createButton("Translate", CC_CALLBACK_1(HelloWorld::translateTriangle, this));
    auto triangleHideButton = createButton("Hide", CC_CALLBACK_1(HelloWorld::hideTriangle, this));

    // Positioning parameters
    const float panelVertivalOffset = 30;
    const float panelHorizontalOffset = 10;
	auto panelSpacing = (panel->getContentSize().width - panelHorizontalOffset * 2 - triangleRotateButton->getContentSize().width * 4) / 3;


    if (triangleRotateButton)
    {
        const float x = origin.x + panelHorizontalOffset;
        const float y = origin.y + panelVertivalOffset;
    	triangleRotateButton->setPosition(x,y);
		panelMenu->addChild(triangleRotateButton);
    }

    if (triangleScaleButton)
    {
	    const float x = origin.x + panelHorizontalOffset + panelSpacing + triangleScaleButton->getContentSize().width;
    	const float y = origin.y + panelVertivalOffset;
    	triangleScaleButton->setPosition(x,y);
		panelMenu->addChild(triangleScaleButton);
    }

    if (triangleTranslateButton)
    {
	    const float x = origin.x + panelHorizontalOffset + panelSpacing * 2 + triangleTranslateButton->getContentSize().width * 2;
    	const float y = origin.y + panelVertivalOffset;
    	triangleTranslateButton->setPosition(x,y);
		panelMenu->addChild(triangleTranslateButton);
    }

    if (triangleHideButton)
    {
	    const float x = origin.x + panelHorizontalOffset + panelSpacing * 3 + triangleHideButton->getContentSize().width * 3;
		const float y = origin.y + panelVertivalOffset;
	    triangleHideButton->setPosition(x,y);
		panelMenu->addChild(triangleHideButton);
    }
    


    // Create Hello World
    auto title = Label::createWithTTF("Hello World", "fonts/Times New Roman.ttf", 42, 
		Size::ZERO, TextHAlignment::CENTER, TextVAlignment::CENTER);
    if (!title)
        problemLoading("'fonts/Times New Roman.ttf'");
    else
    {
	    title->setPosition(Vec2(visibleSize.width/2, visibleSize.height - title->getContentSize().height));
        this->addChild(title);
    }

    setupPlayerInput();

    return true;
}


cocos2d::MenuItemSprite* HelloWorld::createButton(const std::string& labelText, const cocos2d::ccMenuCallback& callback)
{
    auto button = MenuItemImage::create("PanelButton.png", "PanelButtonPressed.png");

    if (!button || button->getContentSize().width <= 0 || button->getContentSize().height <= 0)
        problemLoading("'PanelButton.png' and 'PanelButtonPressed.png'");
    else
    {
        button->setAnchorPoint(Vec2::ZERO);
        button->setCallback(callback);

        // Create label and position it in center
        auto label = Label::createWithTTF(labelText, "fonts/Times New Roman.ttf", 30,
            Size::ZERO, TextHAlignment::CENTER, TextVAlignment::CENTER);
        label->setPosition(button->getContentSize() / 2);
        label->setColor(Color3B::BLACK);
        button->addChild(label);        
    }
	return button;
}

void HelloWorld::setupPlayerInput()
{
	// Create listener on change window resolution
    auto changeResolutionListener = EventListenerKeyboard::create();
    changeResolutionListener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onChangeResolutionEvent, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(changeResolutionListener,this);

    // Create listener on close application
    auto closeAppListener = EventListenerKeyboard::create();
    closeAppListener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onCloseEvent, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(closeAppListener,this);
}


bool HelloWorld::onChangeResolutionEvent(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event)
{
    static const auto defaultResolution = Size(1280,720);
    static const auto altResolution = Size(960,540);

	if (keyCode == EventKeyboard::KeyCode::KEY_SPACE)
	{
		const auto glView = Director::getInstance()->getOpenGLView();
		if (glView->getFrameSize().height > altResolution.height)
		{
            glView->setFrameSize(altResolution.width, altResolution.height);
		}
        else
        {
            glView->setFrameSize(defaultResolution.width, defaultResolution.height);
        }
	}
    return true;
}


bool HelloWorld::onCloseEvent(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event)
{
    //Close the cocos2d-x game scene and quit the application
    if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
		Director::getInstance()->end();
    return true;
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
}

void HelloWorld::togglePanelCallback(Ref* pSender)
{
	if (triangle->isVisible() && panel->isVisible())
	{
		triangle->setVisible(false);
        panel->setVisible(false);
	}
    else
    {
	    triangle->setVisible(true);
        panel->setVisible(true);
    }
}

void HelloWorld::rotateTriangle(Ref* pSender)
{
    static bool isActivated;

    auto action = RotateTo::create(1.5f, isActivated ? 0.f : 90.f);
    action->setTag(1);
    isActivated = !isActivated;

    triangle->stopActionByTag(1);
    triangle->runAction(action);
}

void HelloWorld::scaleTriangle(Ref* pSender)
{
    static bool isActivated;

    auto action = ScaleTo::create(1.f, isActivated ? 1.f : 2.f);
	action->setTag(2);
    isActivated = !isActivated;

    triangle->stopActionByTag(2);
    triangle->runAction(action);
}

void HelloWorld::translateTriangle(Ref* pSender)
{
    static bool isActivated;
    static const auto defaultLocation = triangle->getPosition();
    const auto moveLocation = Vec2(defaultLocation.x, defaultLocation.y + triangle->getContentSize().height * 2 * triangle->getScale());

    auto action = MoveTo::create(0.5f, isActivated ? defaultLocation : moveLocation);
	action->setTag(3);
    isActivated = !isActivated;

    triangle->stopActionByTag(3);
    triangle->runAction(action);
}

void HelloWorld::hideTriangle(Ref* pSender)
{
    static bool isActivated;
    const auto hidden = 0;
    const auto visible = 255;

    auto action = FadeTo::create(5.f, isActivated ? visible : hidden);
    action->setTag(4);
    isActivated = !isActivated;

    triangle->stopActionByTag(4);
    triangle->runAction(action);
}

